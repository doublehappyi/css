/**
 * Created by db on 15-4-20.
 */
var gulp = require("gulp"),
    prefixer = require("gulp-autoprefixer");

gulp.task("flex", function(){
    gulp.src("flex/src.css")
        .pipe(prefixer())
        .pipe(gulp.dest("./flex/build"))
});